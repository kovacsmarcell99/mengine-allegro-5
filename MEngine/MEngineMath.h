#ifndef MENGINEMATH_H
#define MENGINEMATH_H

#include <cmath>

#define PI 3.141592653589793238462

float toRad(float angle)
{
	return ((angle * PI) / 180);
}
float toAngle(float rad)
{
	return ((rad * 180) / PI);
}

#endif