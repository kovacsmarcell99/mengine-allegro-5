/*
            ____                                                 
 /'\_/'\  /\  _ \                     __						 
/\      \ \ \ \_      ___       __   /\_\     ___       __		 
\ \ \__\ \ \ \  _\  /' _ `\   /'_ `\ \/\ \  /' _ `\   /'__`\	 
 \ \ \_/\ \ \ \ \__ /\ \/\ \ /\ \L\ \ \ \ \ /\ \/\ \ /\  __/	 
  \ \_\\ \_\ \ \___\\ \_\ \_\\ \____ \ \ \_\\ \_\ \_\\ \____\	 
   \/_/ \/_/  \/___/ \/_/\/_/ \/___L\ \ \/_/ \/_/\/_/ \/____/	 
                                   /\____/                       
                                   \_/__/		      			 

											Copyright Marcell Games 2013
*/
#ifndef MENGINE_H
#define MENGINE_H

//Include Allegro5 Header Files
#include <allegro5/allegro.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_native_dialog.h>

//OPENGL Header Files
#include <allegro5/allegro_opengl.h>

//C Libs
#include <ctime>
#include <cstdlib>

//MEngine Version
#include "MEngineVersion.h"

//Random
#include "Random.h"

//Include Position Stuff
#include "Vertex.h"
#include "Quaternion.h"
#include "Matrix4.h"
#include "Vector3.h"
#include "Vector2.h"
#include "Rect.h"
#include "Point.h"

//Include Draw Stuff
#include "DrawManager.h"
#include "Texture2D.h"
#include "ImageManager.h"

//Options
#include "MEngineOptions.h"

//Math Library to extend standard C/C++ math
#include "MEngineMath.h"

//OpenGL Stuff
#include "MEngineOpenGL.h"
#include "Mesh.h"

//MEngine Version
//Now Defined in MEngineOptions.h!

//Window
#include "Window.h"

//Input
#include "Input.h"

//Audio
#include "SoundEffect.h"
#include "AudioManager.h"

//ALLEGRO 5 INIT FUNCTIONS
bool MEngineInit()
{
	try
	{
		if(!al_init())
			throw "Couldn't initialize Allegro 5!";
		
		if(!al_install_keyboard())
			throw "Couldn't install keyboard!";
		if(!al_install_mouse())
			throw "Couldn't install mouse!";
		if(!al_install_audio())
			throw "Couldn't install audio!";

		if(!al_init_image_addon())
			throw "Couldn't initialize image addon!";
		if(!al_init_acodec_addon())
			throw "Couldn't initialize acodec addon!";

		//Font addon doesn't return a bool value
		al_init_font_addon();
		//TTF addon can only be run after font addon!
		if(!al_init_ttf_addon())
			throw "Couldn't initialize True Type Font addon!";
	}
	catch(char* errorMessage)
	{
		al_show_native_message_box(NULL,"Error!","Error!",errorMessage,NULL,NULL);
		return false;
	}
	return true;
}

#endif