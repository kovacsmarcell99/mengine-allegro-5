#ifndef KEYBOARD_H
#define KEYBOARD_H

#include <allegro5/allegro.h>

class Keyboard
{
public:
	Keyboard()
	{
		ClearKeys();
	}
	Keyboard(ALLEGRO_KEYBOARD_STATE keyState)
	{
		this->keyState = keyState;
		ClearKeys();
	}

	bool IsButtonPressed(int button)
	{
		if(keys[button])
			return true;
		else
			return false;
	}

	//Setters

	void Update()
	{
		//Update Keystate
		al_get_keyboard_state(&keyState);

		for(int i = 0;i < ALLEGRO_KEY_MAX;i++)
		{
			if(al_key_down(&keyState,i))
				keys[i] = true;
			else
				keys[i] = false;
		}
	}

	void SetKeyState(ALLEGRO_KEYBOARD_STATE keyState)
	{
		this->keyState = keyState;
	}

	void ClearKeys()
	{
		for(int i = 0;i < ALLEGRO_KEY_MAX;i++)
		{
			keys[i] = 0;
		}
	}
private:
	ALLEGRO_KEYBOARD_STATE keyState;
	bool keys[ALLEGRO_KEY_MAX];
};

#endif