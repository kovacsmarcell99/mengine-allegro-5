#ifndef TEXTURE2D_H
#define TEXTURE2D_H

#include <allegro5/allegro.h>

class Texture2D
{
public:
	Texture2D()
	{
		image = NULL;
	}
	Texture2D(const char* filename)
	{
		image = al_load_bitmap(filename);
	}

	ALLEGRO_BITMAP* GetBitmap()
	{
		return image;
	}

	~Texture2D()
	{
		al_destroy_bitmap(image);
	}
private:
	ALLEGRO_BITMAP *image;
};

#endif