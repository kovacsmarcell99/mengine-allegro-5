#ifndef IMAGEMANAGER_H
#define IMAGEMANAGER_H

#include <vector>
#include <map>
#include <string>

#include <allegro5/allegro.h>

class ImageManager
{
public:
	ImageManager()
	{

	}
	ImageManager(ALLEGRO_DISPLAY *display)
	{
		CreateMissingTexture(display);
	}
	~ImageManager()
	{
		Clear();
		al_destroy_bitmap(textureMissing);
	}

	void CreateMissingTexture(ALLEGRO_DISPLAY *display)
	{
		textureMissing = al_create_bitmap(64, 64);

		al_set_target_bitmap(textureMissing);
		al_clear_to_color(al_map_rgb(255, 0, 255));
		al_set_target_bitmap(al_get_backbuffer(display));

		if (textureMissing == NULL)
		{
			al_show_native_message_box(NULL, "Error!", "Error!", "Couldn't create missing texture!", NULL, NULL);
		}
	}

	//Bitmap Fuctions

	void AddBitmap(std::string key,char *filepath)
	{
		if (images[imageKeys[key]] == NULL)
		{
			images.push_back(al_load_bitmap(filepath));
			imageKeys[key] = (images.size() - 1);

			if (images[imageKeys[key]] == NULL)
				al_show_native_message_box(NULL, "Error!", "Error!", "Couldn't load Image or \n Image is empty!", NULL, NULL);
		}
		else
			al_show_native_message_box(NULL, "Error!", "Error!", "There is already an Image \n with that key!", NULL, NULL);
	}

	ALLEGRO_BITMAP* GetBitmap(std::string key)
	{
		if (images[imageKeys[key]] != NULL)
			return images[imageKeys[key]];
		else
		{
			return textureMissing;
		}
	}
	
	void DeleteBitmap(std::string key)
	{
		if (images[imageKeys[key]] != NULL)
			al_destroy_bitmap(images[imageKeys[key]]);
		else
			al_show_native_message_box(NULL, "Error!", "Error!", "Couldn't DeleteImage(); \n probably no picture is loaded!", NULL, NULL);
	}

	//Draw Functions

	//Normal Draw
	void DrawBitmap(std::string key, int x, int y)
	{
		al_draw_bitmap(GetBitmap(key), x, y, NULL);
	}
	void DrawBitmap(std::string key,int x,int y,int flags)
	{
		al_draw_bitmap(GetBitmap(key), x, y, flags);
	}

	template<class U>
	void DrawBitmap(std::string key,U pos)
	{
		al_draw_bitmap(GetBitmap(key), pos.x, pos.y, NULL);
	}

	template<class U>
	void DrawBitmap(std::string key, U pos,int flags)
	{
		al_draw_bitmap(GetBitmap(key), pos.x, pos.y,flags)
	}

	//TODO: SCALED DRAW
	//TODO: ROTATED DRAW
	//TODO: DRAW REGION

	//TODO: COMBINED DRAWING

	void Clear()
	{
		for(unsigned int i = 0; i < images.size(); i++)
		{
			al_destroy_bitmap(images[i]);
		}
	}
private:
	std::map<std::string,unsigned int> imageKeys;
	std::vector<ALLEGRO_BITMAP*> images;
	ALLEGRO_BITMAP *textureMissing;
};

#endif