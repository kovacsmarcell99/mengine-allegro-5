#ifndef MESH_H
#define MESH_H

#include <allegro5/allegro_opengl.h>

#include "Vector2.h"
#include "Vector3.h"
#include "Vertex.h"

class Mesh
{
public:
	Mesh()
	{
		glGenBuffers(1,&vbo);
		size = 0;
	}

	void AddVertices(Vertex* vertices, int vertSize, int* indices, int indexSize, bool calcNormals)
	{
        size = indexSize;

        if(calcNormals)
                this->calcNormals(vertices, vertSize, indices, indexSize);

        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, vertSize * sizeof(Vertex), vertices, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexSize * sizeof(int), indices, GL_STATIC_DRAW);
	}

	void Draw()
	{
			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);
			glEnableVertexAttribArray(2);

			glBindBuffer(GL_ARRAY_BUFFER, vbo);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)sizeof(Vector3));
			glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(sizeof(Vector3) + sizeof(Vector2)));

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
			glDrawElements(GL_TRIANGLES, size, GL_UNSIGNED_INT, 0);

			glDisableVertexAttribArray(0);
			glDisableVertexAttribArray(1);
			glDisableVertexAttribArray(2);
	}

private:
	void calcNormals(Vertex* vertices, int vertSize, int* indices, int indexSize)
	{
			for(int i = 0; i < indexSize; i += 3)
			{
					int i0 = indices[i];
					int i1 = indices[i + 1];
					int i2 = indices[i + 2];
                        
					Vector3 v1 = vertices[i1].pos - vertices[i0].pos;
					Vector3 v2 = vertices[i2].pos - vertices[i0].pos;
                
					Vector3 normal = v1.Cross(v2).Normalize();
	
                
					vertices[i0].normal += normal;
					vertices[i1].normal += normal;
					vertices[i2].normal += normal;
			}
        
			for(int i = 0; i < vertSize; i++)
					vertices[i].normal.Normalize();
	}

	unsigned int vbo;
	int size;
	int ibo;
};

#endif