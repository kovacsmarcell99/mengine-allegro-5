#ifndef MOUSE_H
#define MOUSE_H

#include <allegro5/allegro.h>
#include "Vector2.h"
#include "Rect.h"

enum MouseButton{LEFT = 1,	RIGHT = 2};

class Mouse
{
public:
	Mouse()
	{
		left = right = false;
		ClearMousePosition();
	}
	Mouse(ALLEGRO_MOUSE_STATE mouseState)
	{
		this->mouseState = mouseState;
		left = right = false;
		ClearMousePosition();
	}

	bool isButtonPressed(int button)
	{
		if(button == 1)
			return left;
		else if(button == 2)
			return right;
		
		return false;
	}

	Rect GetMousePosRect()
	{
		return mousePos;
	}
	Vector2 GetMousePosVector2()
	{
		return Vector2::CreateVector(mousePos.getX(),mousePos.getY());
	}

	int getX()
	{
		return mousePos.getX();
	}
	int getY()
	{
		return mousePos.getY();
	}
	int getW()
	{
		return mousePos.getW();
	}
	int getH()
	{
		return mousePos.getH();
	}
	
	
	void MousePositionCorrector(int ScreenWidth,int ScreenHeight)
	{
		int x = mouseState.x;
		if(x <= ScreenWidth && x > 0)
			mousePos.x = mouseState.x;

		int y = mouseState.y;
		if(y <= ScreenHeight && y > 0)
			mousePos.y = mouseState.y;
	}

	void Update(int ScreenWidth,int ScreenHeight)
	{
		al_get_mouse_state(&mouseState);

		//Calculate if buttons are pressed
		if(al_mouse_button_down(&mouseState, 1))
			left = true;
		else
			left = false;

		if(al_mouse_button_down(&mouseState, 2))
			right = true;
		else
			right = false;

		//Correct Mouse Position
		MousePositionCorrector(ScreenWidth,ScreenHeight);
	}

	void ClearMousePosition()
	{
		this->mousePos.setX(0);	
		this->mousePos.setY(0);
		this->mousePos.setW(0);
		this->mousePos.setH(0);
	}

	void SetMouseState(ALLEGRO_MOUSE_STATE mouseState)
	{
		this->mouseState = mouseState;
	}
private:
	ALLEGRO_MOUSE_STATE mouseState;
	bool left,right;
	Rect mousePos;
};

#endif