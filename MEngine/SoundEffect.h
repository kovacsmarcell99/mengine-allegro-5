#ifndef SOUNDEFFECT_H
#define SOUNDEFFECT_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

class SoundEffect
{
public:
	SoundEffect()
	{
		soundEffect = NULL;
	}

	SoundEffect(ALLEGRO_SAMPLE *soundEffect)
	{
		this->soundEffect = soundEffect;
	}

	void Load(ALLEGRO_SAMPLE *soundEffect)
	{
		this->soundEffect = soundEffect;
	}

	void Play()
	{
		al_play_sample(soundEffect, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, &id);
	}
	void Play(float gain,float pan,float speed,ALLEGRO_PLAYMODE loop)
	{
		al_play_sample(soundEffect, gain, pan, speed, loop, &id);
	}

	~SoundEffect()
	{
		al_destroy_sample(soundEffect);
	}

	ALLEGRO_SAMPLE *soundEffect;
	ALLEGRO_SAMPLE_ID id;
};

#endif