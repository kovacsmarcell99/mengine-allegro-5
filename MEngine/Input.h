#ifndef INPUT_H
#define INPUT_H

#include <allegro5/allegro.h>

#include "Mouse.h"
#include "Keyboard.h"

class Input
{
public:
	Mouse mouse;
	Keyboard keyboard;
	ALLEGRO_EVENT event;
};

#endif