#ifndef DRAWMANAGER_H
#define DRAWMANAGER_H

#include <allegro5/allegro.h>

#include "Texture2D.h"
#include "Rect.h"
#include "Vector2.h"
#include "Point.h"

class DrawManager
{
public:
	DrawManager();
	~DrawManager();

	template<class U>
	void Draw(Texture2D texture,U pos)
	{
		al_draw_bitmap(texture.image,pos.x,pos.y,NULL);
	}

	template<class U>
	void Draw(ALLEGRO_BITMAP* texture,U pos)
	{
		al_draw_bitmap(texture,pos.x,pos.y,NULL);
	}
};

#endif