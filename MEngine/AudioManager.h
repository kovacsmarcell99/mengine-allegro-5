#ifndef AUDIOMANAGER_H
#define AUDIOMANAGER_H

#include <map>
#include <vector>
#include <string>

#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

class AudioManager
{
public:
	AudioManager()
	{
		std::cout << "Starting AudioManager..." << std::endl;
	}
	~AudioManager()
	{
		Clear();
		std::cout << "Stoping AudioManager..." << std::endl;
	}

	//SoundEffect
	void AddSoundEffect(std::string key, char *filepath)
	{
		if (soundEffectKeys[key] == NULL)
		{
			std::cout << "Adding SoundEffect " << filepath << " with key " << key.c_str() << std::endl;
			soundEffects.push_back(al_load_sample(filepath));
			soundEffectKeys[key] = (soundEffects.size() - 1);
			ALLEGRO_SAMPLE_ID id;
			soundEffectID.push_back(id);

			if (soundEffects[soundEffectKeys[key]] == NULL)
				al_show_native_message_box(NULL, "Error!", "Error!", "Couldn't load SoundEffect or \n SoundEffect is empty!", NULL, NULL);
		}
		else
			al_show_native_message_box(NULL, "Error!", "Error!", "There is already a SoundEffect \n with that key!", NULL, NULL);
	}
	void DeleteSoundEffect(std::string key)
	{
		std::cout << "Deleting SoundEffect" << " with key " << key.c_str() << std::endl;
		if (soundEffects[soundEffectKeys[key]] != NULL)
			al_destroy_sample(soundEffects[soundEffectKeys[key]]);
		else
			al_show_native_message_box(NULL, "Error!", "Error!", "Couldn't DeleteSoundEffect(); \n probably no Soundeffect is loaded!", NULL, NULL);
	}

	ALLEGRO_SAMPLE* GetSoundEffect(std::string key)
	{
		if (soundEffects[soundEffectKeys[key]] != NULL)
			return soundEffects[soundEffectKeys[key]];
		else
			return NULL;
	}

	void PlaySoundEffect(std::string key)
	{
		//if (soundEffects[soundEffectKeys[key]] != NULL)
			al_play_sample(soundEffects[soundEffectKeys[key]], 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, &soundEffectID[soundEffectKeys[key]]);
	}
	void PlaySoundEffect(std::string key, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop)
	{
		//if (soundEffects[soundEffectKeys[key]] != NULL)
			al_play_sample(soundEffects[soundEffectKeys[key]], gain, pan, speed, loop, &soundEffectID[soundEffectKeys[key]]);
	}

	//Music
	void AddMusic(std::string key, char *filepath)
	{
		if (musicKeys[key] == NULL)
		{
			std::cout << "Adding Music " << filepath << " with key " << key.c_str() << std::endl;
			music.push_back(al_load_sample(filepath));
			musicKeys[key] = (music.size() - 1);

			musicInstance.push_back(al_create_sample_instance(music[musicKeys[key]]));

			if (music[musicKeys[key]] == NULL)
				al_show_native_message_box(NULL, "Error!", "Error!", "Couldn't load Music or \n Music is empty!", NULL, NULL);

			if (musicInstance[musicKeys[key]] == NULL)
				al_show_native_message_box(NULL, "Error!", "Error!", "Couldn't load MusicInstance or \n MusicInstance is empty!", NULL, NULL);
		}
		else
			al_show_native_message_box(NULL, "Error!", "Error!", "There is already a Music \n with that key!", NULL, NULL);
	}
	void DeleteMusic(std::string key)
	{
		std::cout << "Deleting Music with key " << key.c_str() << std::endl;
		if (music[musicKeys[key]] != NULL)
		{
			al_destroy_sample(music[musicKeys[key]]);
			al_destroy_sample_instance(musicInstance[musicKeys[key]]);
		}
		else
			al_show_native_message_box(NULL, "Error!", "Error!", "Couldn't DeleteMusic(); \n probably no Music is loaded!", NULL, NULL);

	}

	ALLEGRO_SAMPLE_INSTANCE* GetMusic(std::string key)
	{
		if (music[musicKeys[key]] != NULL)
			return musicInstance[musicKeys[key]];
		else
			return NULL;
	}

	void SetPlayMode(std::string key,ALLEGRO_PLAYMODE playmode)
	{
		std::cout << "Setting Music Playmode with key " << key.c_str() << " to playmode " << playmode << std::endl;
		if (music[musicKeys[key]] != NULL)
		{
			al_set_sample_instance_playmode(musicInstance[musicKeys[key]], playmode);
			al_attach_sample_instance_to_mixer(musicInstance[musicKeys[key]], al_get_default_mixer());
		}
		else
			al_show_native_message_box(NULL, "Error!", "Error!", "Can't set Playmode probably there \n is no Music loaded!", NULL, NULL);
	}

	void PlayMusic(std::string key)
	{
		if (music[musicKeys[key]] != NULL)
			al_play_sample_instance(musicInstance[musicKeys[key]]);
		else
			al_show_native_message_box(NULL, "Error!", "Error!", "Can't play Music probably there \n is no Music loaded!", NULL, NULL);
	}
	void StopMusic(std::string key)
	{
		if (music[musicKeys[key]] != NULL)
			al_stop_sample_instance(musicInstance[musicKeys[key]]);
		else
			al_show_native_message_box(NULL, "Error!", "Error!", "Can't play Music probably there \n is no Music loaded!", NULL, NULL);
	}
	//Both
	void Clear()
	{
		std::cout << "Clearing SoundEffects" << std::endl;
		for (unsigned int i = 0; i < soundEffects.size(); i++)
		{
			al_destroy_sample(soundEffects[i]);
		}
		std::cout << "Clearing Musics" << std::endl;
		for (unsigned int i = 0; i < music.size(); i++)
		{
			al_destroy_sample(music[i]);
			al_destroy_sample_instance(musicInstance[i]);
		}
	}
	
	void AutoReserve()
	{
		std::cout << "Auto Reveserving " << soundEffects.size() << " SoundEffect and " << music.size() << " Music" << std::endl;
		al_reserve_samples(soundEffects.size() + music.size());
	}
	void ManualReserve(int num)
	{
		std::cout << "Manual Reveserving " << num << " Samples" << std::endl;
		al_reserve_samples(num);
	}
private:
	std::map<std::string, unsigned int> soundEffectKeys;
	std::vector<ALLEGRO_SAMPLE*> soundEffects;
	std::vector<ALLEGRO_SAMPLE_ID> soundEffectID;

	std::map<std::string, unsigned int> musicKeys;
	std::vector<ALLEGRO_SAMPLE*> music;
	std::vector<ALLEGRO_SAMPLE_INSTANCE*> musicInstance;
private:
};

#endif