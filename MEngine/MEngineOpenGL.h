#ifndef MENGINEOPENGL_H
#define MENGINEOPENGL_H

#include <allegro5/allegro_opengl.h>

void ClearScreen()
{
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	//TODO: Stencil Buffer
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
//	glLoadIdentity();  
}

void InitGraphics()
{
	glEnable(GL_DEPTH_TEST);

    glViewport(0.0f,0.0f,800.0f,600.0f);

    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();

    glOrtho(0.0f,800.0f,600.0f,0.0f,-200.0f,200.0f);
    glClearColor(0.0f,0.0f,0.0f,1.0f);

	glMatrixMode(GL_MODELVIEW);
}

#endif