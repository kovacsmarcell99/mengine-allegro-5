#ifndef MENGINEOPTIONS_H
#define MEGNINEOPTIONS_H

#include "MEngineVersion.h"

#include <iostream>
#include <fstream>
#include <string>
#include <map>

#include <allegro5/allegro_native_dialog.h>

//MEngine Options Version
#define EngineOptionsVersion 1.0

class MEngineOptions
{
public:
	MEngineOptions()
	{
		in.open("options.txt");
	}
	MEngineOptions(const char* filename)
	{
		in.open(filename);
	}
	~MEngineOptions()
	{
		in.close();
	}


	void ReadRow()
	{
		std::string name	=	"";
		double info			=	0;

		in >> name;
		in >> info;

		data[name] = info;
	}
	void ReadRows(int num)
	{
		if(in.is_open())
		{
			for(int i = 0;i < num;i++)
				ReadRow();
		}
	}

	bool CompareEngineOptionsVersion()
	{	
		double version = data["EngineOptionsVersion"];

		if(version == EngineOptionsVersion)
			return true;
		else if(version > EngineOptionsVersion)
		{
			al_show_native_message_box(NULL,"WARNING!","Warning!","The Options Loader of the Game is better than your options file \n You may not be able to use all your configurated Options and rarely cause problems! \n If you upgrade your game later you will be able to access it! \n \n THIS WON'T RESULT DATA LOSS!!",NULL,NULL);
			return true;
		}
		else if(version < EngineOptionsVersion)
		{
			al_show_native_message_box(NULL,"WARNING!","Warning!","You have older Options Loader version \n than the version of your options file! This may cause serious problems!",NULL,NULL);
			return false;
		}
		
		return false;
	}
	bool CompareEngineVersion()
	{	
		double version = data["EngineVersion"];

		if(version == EngineVersion)
			return true;
		else if(version > EngineVersion)
		{
			al_show_native_message_box(NULL,"WARNING!","Warning!","You have better Engine than it's required \n The Engine should be compatible with older file types \n but errors may occour! \n",NULL,NULL);
			return true;
		}
		else if(version < EngineVersion)
		{
			al_show_native_message_box(NULL,"WARNING!","Warning!","You have old Engine version! \n The game may crash because of newer save types! \n",NULL,NULL);
			return false;
		}
		return false;
	}

	double GetData(std::string dataName)
	{
		return data[dataName];
	}

private:
	std::ifstream in;
	std::map<std::string,double> data;
};

#endif