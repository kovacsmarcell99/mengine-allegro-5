#include "MEngine.h"

#define ScreenWidth 800
#define ScreenHeight 600

int main()
{	
	//Initialize
	if(!MEngineInit())
		return 1;
	
	//Create Display & Set Parameters
	al_set_new_display_flags(ALLEGRO_OPENGL); 

	Window *window = new Window(ScreenWidth,ScreenHeight);
	
	if(!window->isCreated())
	{
		al_show_native_message_box(NULL,"Error","Error!","Couldn't create the display!",NULL,NULL);
		return 1;
	}
	
	//if(optionsRead.GetData("FullScreen") == 0)
		//al_toggle_display_flag(display,ALLEGRO_FULLSCREEN_WINDOW,true);
	
	window->SetTitle("MEngine - Marcell Games");

	ImageManager *images = new ImageManager;
	
	//Create Timer
	float FPS = 60;
	ALLEGRO_TIMER *timer = al_create_timer(1.0f / FPS);

	Input* input = new Input;

	//Create Event Queue & Set Event Sources
	ALLEGRO_EVENT_QUEUE *event_queue = al_create_event_queue();

	al_register_event_source(event_queue,al_get_timer_event_source(timer));
	al_register_event_source(event_queue,al_get_display_event_source(window->display));

	//Start Timer
	al_start_timer(timer);

	float angle = 0;

	bool running = true;

	InitGraphics();

	//Game Loop
	while(running)
	{
		//Event Stuff
		al_wait_for_event(event_queue,&input->event);	

		//Close Window
		if(input->event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
			running = false;

		input->keyboard.Update();
		input->mouse.Update(ScreenWidth,ScreenHeight);

		//On Timer Click
		if(input->event.type == ALLEGRO_EVENT_TIMER)
		{
			//Update

			//Draw
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glPushMatrix();
				glLoadIdentity();

				glTranslatef(200.0f,200.0f,0.0f);
				glRotatef(angle,0.0f,0.0f,1.0f);

				glBegin(GL_QUADS);

				glColor3f(1.0f,1.0f,1.0f);
				glVertex3f(000.0f,000.0f,0.0f);

				glColor3f(1.0f,0.0f,0.0f);
				glVertex3f(200.0f,000.0f,0.0f);
				
				glColor3f(0.0f,1.0f,0.0f);
				glVertex3f(200.0f,200.0f,0.0f);
				
				glColor3f(0.0f,0.0f,1.0f);
				glVertex3f(000.0f,200.0f,0.0f);

				glEnd();
			glPopMatrix();
			
		}
		//Flip Buffer & Clear To Color
		glFlush();
		al_flip_display();
		//al_clear_to_color(al_map_rgb(255,255,255));
		ClearScreen();
		angle += 0.5;
	}

	//Destroy

	delete window;
	delete images;
	delete input;

	al_destroy_timer(timer);
	al_destroy_event_queue(event_queue);

	//Return
	return 0;
}