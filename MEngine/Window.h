#ifndef WINDOW_H
#define WINDOW_H

#include <allegro5/allegro.h>

class Window
{
public:
	Window()
	{
		Create(800, 600);
	}
	Window(int ScreenWidth, int ScreenHeight)
	{
		Create(ScreenWidth, ScreenHeight);
	}
	~Window()
	{
		Destroy();
	}

	void Create(int ScreenWidth, int ScreenHeight)
	{
		if (!isCreated())
		{
			this->ScreenWidth = ScreenWidth;
			this->ScreenHeight = ScreenHeight;

			display = al_create_display(ScreenWidth, ScreenHeight);

			destroyed = false;
		}
	}
	void Destroy()
	{
		if (!destroyed)
		{
			al_destroy_display(display);
			destroyed = true;
		}
	}

	void SetTitle(const char *title)
	{
		al_set_window_title(display, title);
	}
	

	void SetIcon(const char *filename)
	{
		al_set_display_icon(display, al_load_bitmap(filename));
	}
	void SetIcon(ALLEGRO_BITMAP *icon)
	{
		al_set_display_icon(display, icon);
	}

	bool isCreated()
	{
		if (!display)
			return false;
		else
			return true;
	}

	ALLEGRO_DISPLAY *display;
	int ScreenWidth, ScreenHeight;
private:
	bool destroyed;
};

#endif